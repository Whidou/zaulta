#!/usr/bin/python3

# Zaulta: Zaulta Aime Utiliser Librement les Tables Aléatoires
# A Random table roller
# Copyright (C) 2017 Whidou <whidou@openmailbox.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import random
import string
import sys


class Tables:

    def __init__(self, path):
        file_descriptor = open(path, "r")
        self.tables = json.load(file_descriptor)
        file_descriptor.close()
        self.formatter = string.Formatter()

    def __getitem__(self, key):
        if type(key) == slice:
            return list(self.tables.keys())[key]
        if key not in self.tables:
            return key
        return self.formatter.vformat(random.choice(self.tables[key]),
                                      None,
                                      self)


def main():
    if len(sys.argv) != 3:
        print("Usage: {} TABLES FORMAT")
        return

    tables = Tables(sys.argv[1])

    print(tables[sys.argv[2]])


if __name__ == "__main__":
    main()
